#!/bin/bash

RESTART_DELAY=5

[[ ${BASH_VERSINFO[0]} -ge 4 ]] || die "bash ${BASH_VERSINFO[0]} detected, when bash 4+ required"
type streamlink >/dev/null 2>&1 || die "streamlink is not installed. Aborting."

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

usage() {
    cat <<EOM
    Usage:
      $(basename "${BASH_SOURCE[0]}") [-h] [-r retry_delay] [-o output_dir] ttv [-a] CHANNELNAME [QUALITY]
      $(basename "${BASH_SOURCE[0]}") [-h] [-r retry_delay] [-o output_dir] [soop|yt|chz] CHANNELNAME [QUALITY]

    Helper script to record live streams with streamlink.

    Available service providers: [ttv|yt|soop|chz]

    Available common options:
    -h      print this help and exit
    -r      streamlink retry delay
    -o      output directory

    Twitch (ttv) options:
    -a      use authorization headers
EOM
  exit
}

[[ $# -eq 0 ]] && usage

# common args
while getopts ":o:r:h" opt; do
  case $opt in
    o) output_dir=$OPTARG ;;
    r) retry_delay=$OPTARG ;;
    h|*)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

# service provider and associated args
service=$1; shift
case "$service" in
  ttv)
    unset OPTIND
    while getopts ":a" opt; do
      case $opt in
        a) ttv_use_auth=1 ;;
        *)
          usage
          ;;
      esac
    done
    shift $((OPTIND-1))
    ;;
  yt|soop|chz) ;;
  *)
    [[ -n $service ]] && die "Invalid provider: $service"
    ;;
esac

# remaining args
args=("${@}")
[[ ${#args[@]} -eq 0 ]] && die "Missing channel name"
channel="${args[0]}"
quality="${args[1]}"


# Set default output directory and quality
[[ -z $output_dir ]] && output_dir="$HOME/Downloads/"
[[ -z $quality ]] && quality="best"

# Read OAuth Token and Device ID from ~/.twitch_auth
if [[ -n $ttv_use_auth ]]; then
  [[ -f ~/.twitch_auth ]] && . ~/.twitch_auth
  #[[ -z $AUTHTOKEN || -z $DEVICEID ]] && die "Please set AUTHTOKEN and DEVICEID in $HOME/.twitch_auth"
  [[ -z $AUTHTOKEN || -z $DEVICEID || -z $INTEGRITY ]] && die "Please set AUTHTOKEN, DEVICEID and INTEGRITY in $HOME/.twitch_auth"
fi


# build args for streamlink
sl_args=()
sl_args+=("--stream-timeout" 30)
[[ -n $retry_delay ]] && sl_args+=("--retry-streams" "$retry_delay")

# plugin-specific args
case "$service" in
  ttv)
    sl_args+=("--twitch-disable-reruns")
    sl_args+=("--twitch-access-token-param=playerType=site")
    if [[ -n $ttv_use_auth ]]; then
      sl_args+=("--twitch-api-header=Authorization=${AUTHTOKEN}")
      sl_args+=("--twitch-api-header=X-Device-Id=${DEVICEID}")
      sl_args+=("--twitch-api-header=Client-Integrity=${INTEGRITY}")
    fi
    sl_args+=("-o" "$output_dir/twitch/${channel}/rec/{time}_{id}.mp4")
    sl_args+=("https://www.twitch.tv/${channel}")
    ;;
  yt)
    sl_args+=("-o" "$output_dir/youtube/${channel}/rec/{time}_{id}.mp4")
    sl_args+=("--stream-segment-threads" "2")
    sl_args+=("https://www.youtube.com/@${channel}/live")
    ;;
  soop)
    sl_args+=("-o" "$output_dir/soop/${channel}/rec/{time}.mp4")
    sl_args+=("--stream-segment-threads" "10")
    sl_args+=("https://play.sooplive.co.kr/${channel}")
    ;;
  chz)
    sl_args+=("-o" "$output_dir/chzzk/${channel}/rec/{time}.mp4")
    sl_args+=("--stream-segment-threads" "2")
    sl_args+=("--stream-segment-timeout" "15")
    sl_args+=("https://chzzk.naver.com/live/${channel}")
    ;;
  *)
    [[ -n $service ]] && die "Invalid provider: $service"
    ;;
esac
sl_args+=("$quality")


while true
do
  TZ=UTC streamlink "${sl_args[@]}"
  [[ -z "$retry_delay" ]] && exit 0
  sleep $RESTART_DELAY
done
