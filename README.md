# streamrec

Collection of scripts for recording live streams

## Getting Started

### Requirements

For `streamrec`:
* [streamlink](https://streamlink.github.io)
* [optional] `AUTHTOKEN` and `DEVICEID` need to be set in `$HOME/.twitch_auth` in order to use authorization headers for twitch recording

For `chapter_gen`/`chapter_del`:
* [ffmpeg](https://ffmpeg.org/)

### Install

Use git to clone this repository.

```
git clone https://gitlab.com/mehl/streamrec
```

### Usage
#### streamrec
```bash
# record a youtube stream and save to specified path
streamrec -o /path/to/recordings/ yt <yt_channel>

# record a twitch stream (with auth headers)
streamrec ttv -a <twitch_channel>

# constantly check for online status every 60 seconds
streamrec -r 60 ttv -a <twitch_channel>

```

#### chapter_gen/chapter_del
```bash
# read timestamps (unix time) and add chapters to livestream recording
chapter_gen /path/to/video /path/to/timestamps

# remove chapters
chapter_del /path/to/video

```
